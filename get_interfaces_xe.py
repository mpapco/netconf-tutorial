import env_lab
from ncclient import manager
import xml.dom.minidom
import xmltodict

device = env_lab.CISCO_SANDBOX_XE

netconf_filter = """
<filter>
  <interfaces xmlns="urn:ietf:params:xml:ns:yang:ietf-interfaces">
    <interface></interface>
  </interfaces>
</filter>
"""

print(f"Opening NETCONF Connection to {device['host']}")

with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
) as m:

    print('Sending a <get-config> operation to the device.')
    # Make a NETCONF <get-config> query using the filter
    netconf_reply = m.get_config(source='running', filter=netconf_filter)

print("Here is the raw XML data returned from the device.\n")
# Print out the raw XML that returned
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())
print('')

# Parse the returned XML to an Ordered Dictionary
netconf_data = xmltodict.parse(netconf_reply.xml)["rpc-reply"]["data"]

# Create a list of interfaces
interfaces = netconf_data["interfaces"]["interface"]

print("The interface status of the device is: ")
# Loop over interfaces and report status
for interface in interfaces:
    print(f"Interface {interface['name']} status:{interface['enabled']}")
    if 'address' in interface['ipv4']:
        print(f"address: {interface['ipv4']['address']['ip']}/{interface['ipv4']['address']['ip']}")
print("\n")


