import env_lab
from ncclient import manager
import xml.dom.minidom
import xmltodict

device = env_lab.CISCO_SANDBOX_XE

netconf_filter = """
<filter>
<native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
        <ip>
            <prefix-list>
                <prefixes></prefixes>
            </prefix-list>
        </ip>
    </native>
</filter>
"""

print(f"Opening NETCONF Connection to {device['host']}")

with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False
) as m:

    print('Sending a <get-config> operation to the device.')
    # Make a NETCONF <get-config> query using the filter
    netconf_reply = m.get_config(source='running', filter=netconf_filter)

print("Here is the raw XML data returned from the device.\n")
# Print out the raw XML that returned
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())
print('')

# Parse the returned XML to an Ordered Dictionary
netconf_data = xmltodict.parse(netconf_reply.xml)["rpc-reply"]["data"]["native"]

# Create a list of interfaces
prefix_lists = netconf_data["ip"]["prefix-list"]["prefixes"]

print("The prefix list: ")
# Loop over prefixes and report status
for prefix_list in prefix_lists:
    try:
        print(f"{prefix_list['name']} {prefix_list['seq']['no']} {prefix_list['seq']['action']} "
              f"{prefix_list['seq']['ip']} {prefix_list['seq']['ge']} {prefix_list['seq']['le']}")
    except TypeError:
        for odict in range(0, len(prefix_list['seq'])):
            print(f"{prefix_list['name']} {prefix_list['seq'][odict]['no']} {prefix_list['seq'][odict]['action']} "
                  f"{prefix_list['seq'][odict]['ip']} {prefix_list['seq'][odict]['ge']} "
                  f"{prefix_list['seq'][odict]['le']}")
    print('-' * 50)




