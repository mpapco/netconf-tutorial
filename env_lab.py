CISCO_SANDBOX_XE = {
    "host": "ios-xe-mgmt.cisco.com",
    "username": "developer",
    "password": "C1sco12345",
    "netconf_port": 10000,
    "restconf_port": 9443,
    "ssh_port": 8181,
}

CISCO_SANDBOX_XR = {
    "host": "sbx-iosxr-mgmt.cisco.com",
    "username": 'admin',
    "password": 'C1sco12345',
    "netconf_port": 10000,
    "bash_port": 8282,
    "ssh_port": 8181,
    "device_params": {'name': 'csr'}
}

SW04 = {
    "host": "10.24.10.4",
    "username": 'auto',
    "password": 'Heslo01',
    "netconf_port": 830,
    "bash_port": 8282,
    "ssh_port": 22,
}
