from ncclient import manager
import xml.dom.minidom

import env_lab

device = env_lab.CISCO_SANDBOX_XE

netconf_prefixlist_template = """
<config>
    <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
        <ip>
            <prefix-list>
                <prefixes>
                    <name>{name}</name>
                    <description>{desc}</description>
                    <seq>
                        <no>{seq_num}</no>
                        <action>{action}</action>
                        <ip>{ip_prefix}</ip>
                        <ge>{ge}</ge>
                        <le>{le}</le>
                    </seq>
                </prefixes>
            </prefix-list>
        </ip>
    </native>
</config>
"""

new_prefixlist = {
    'name': 'PL_' + input('Enter prefix list name: '),
    'desc': 'Created by Automated script',
    'seq_num': input('Enter sequence number: '),
    'action': input('Enter action (permit/deny): '),
    'ip_prefix': input('Specify ip prefix (e.g. 10.1.1.0/25): '),
    'ge': input('Specify ge(1-32): '),
    'le': input('Specify le(1-32): '),
}


# Create netconf data format for interface
netconf_data = netconf_prefixlist_template.format(
    name=new_prefixlist['name'],
    desc=new_prefixlist['desc'],
    seq_num=new_prefixlist['seq_num'],
    action=new_prefixlist['action'],
    ip_prefix=new_prefixlist['ip_prefix'],
    ge=new_prefixlist['ge'],
    le=new_prefixlist['le'],
)

print(f'Config payload to be sent over NETCONF\n {netconf_data}')
print(f'Opening NETCONF connection to {device}')

# Open connection to device with ncclient
with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
) as m:

    print('Sending <edit-config> to device.')
    netconf_reply = m.edit_config(netconf_data, target='running')

print("Here is the raw XML data returned from the device.\n")
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())
