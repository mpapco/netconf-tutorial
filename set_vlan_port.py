from ncclient import manager
import xml.dom.minidom

import env_lab

device = env_lab.SW04

netconf_vlanport_cisco_template = """
<config>
    <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
        <interface>
            <GigabitEthernet operation="replace">
                <name>1/0/20</name>
                <description>== AVAILABLE FOR FUTURE USE ==</description>
                <switchport-wrapper>
                    <switchport>
                        <mode>access</mode>
                        <access>
                            <vlan>299</vlan>
                        </access>
                    </switchport>
                </switchport-wrapper>
            </GigabitEthernet>
        </interface>
        <interface>
            <GigabitEthernet operation="replace">
                <name>1/0/21</name>
                <description>== AVAILABLE FOR FUTURE USE ==</description>
                <switchport-wrapper>
                    <switchport>
                        <mode>trunk</mode>
                        <trunk>
                            <allowed>
                                <vlan>
                                    <vlans>200,299</vlans>
                                </vlan>
                            </allowed>
                            <native>
                                <vlan>299</vlan>
                            </native>
                        </trunk>
                    </switchport>
                </switchport-wrapper>
            </GigabitEthernet>
        </interface>
    </native>
</config>
"""

netconf_vlanport_oc_template = """
<config>
    <interfaces xmlns="http://openconfig.net/yang/interfaces">
        <interface>
            <name>GigabitEthernet1/0/20</name>
            <config>
                <description>== AVAILABLE FOR FUTURE USE (AUTO) ==</description>
            </config>
            <ethernet xmlns="http://openconfig.net/yang/interfaces/ethernet">
                <switched-vlan xmlns="http://openconfig.net/yang/vlan">
                    <config operation="replace">
                        <interface-mode>ACCESS</interface-mode>
                        <access-vlan>201</access-vlan>
                    </config>
                </switched-vlan>
            </ethernet>
        </interface>
        <interface>
            <name>GigabitEthernet1/0/21</name>
            <config>
                <description>== AVAILABLE FOR FUTURE USE (AUTO) ==</description>
            </config>
            <ethernet xmlns="http://openconfig.net/yang/interfaces/ethernet">
                <switched-vlan xmlns="http://openconfig.net/yang/vlan">
                    <config operation="replace">
                        <interface-mode>TRUNK</interface-mode>
                        <trunk-vlans>200..220</trunk-vlans>
                        <native-vlan>201</native-vlan>
                    </config>
                </switched-vlan>
            </ethernet>
        </interface>
    </interfaces>
</config>
"""

# Create netconf data format for interface
netconf_data = netconf_vlanport_oc_template .format()
# netconf_data = netconf_vlanport_cisco_template .format()

print(f'Config payload to be sent over NETCONF\n {netconf_data}')
print(f'Opening NETCONF connection to {device}')

# Open connection to device with ncclient
with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
) as m:

    print('Sending <edit-config> to device.')
    netconf_reply = m.edit_config(netconf_data, target='running')

print("Here is the raw XML data returned from the device.\n")
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())
