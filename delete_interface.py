from ncclient import manager
import xml.dom.minidom

import env_lab

device = env_lab.CISCO_SANDBOX_XE

# IETF Interface Types
INTERFACE_TYPES = {
    'loopback': 'ianaift:softwareLoopback',
    'ethernet': 'ianaift:ethernetCsmacd',
    'framerelay': 'ianaift:frameRelay',
    'tunnel': 'ianaift:tunnel',
    'vlan': 'ianaift:l2vlan',
    'l3vlan': 'ianaift:l3ipvlan',
    'mplstunnel': 'ianaift:mplsTunnel',
    'mpls': 'ianaift:mpls',
}

# XML configuration template
netconf_interface_template = """
<config>
    <interfaces xmlns="urn:ietf:params:xml:ns:yang:ietf-interfaces">
        <interface operation="delete">
            <name>{name}</name>
        </interface>
    </interfaces>
</config>
"""

new_loopback = {
    'name': 'Loopback' + input('Enter loopback interface number to delete: ')
}

# Create netconf data format for interface
netconf_data = netconf_interface_template.format(
    name=new_loopback['name'],
)

print(f'Config payload to be sent over NETCONF\n {netconf_data}')
print(f'Opening NETCONF connection to {device}')

with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
) as m:

    print('Sending delete operation to device.')
    netconf_reply = m.edit_config(netconf_data, target='running')

print("Here is the raw XML data returned from the device.\n")
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())