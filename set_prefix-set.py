from ncclient import manager, xml_
import xml.dom.minidom
import yaml

import env_lab

device = env_lab.CISCO_SANDBOX_XR

# with open('set_prefix-set.yaml', 'r') as ps_file:
#     data = yaml.load(ps_file, Loader=yaml.FullLoader)
#     for block in data:
#         # print(f'block: {block}')
#         for ps_name, ps_value in block.items():
#             # print(f'ps_name: {ps_name}\nps_value: {ps_value}')
#             if ps_name == 'prefix_set_name':
#                 print(f'ps_name: {ps_value}')
#             elif ps_name == 'prefixes':
#                 for prefix in ps_value:
#                     # print(f'prefix: {prefix}')
#                     for key, value in prefix.items():
#                         if key == 'prefix':
#                             print(f'\t {key}: {value}', end='')
#                         elif key == 'mask':
#                             print(f'\t {key}: {value}')

# XML body to set specific prefix-set
netconf_prefixset_template = """
<config>
    <routing-policy xmlns="http://openconfig.net/yang/routing-policy">
        <defined-sets>
            <prefix-sets>
                <prefix-set operation="create">
                    <prefix-set-name>{name}</prefix-set-name>
                    <prefixes>
                    
                        <prefix>
                            <ip-prefix>{prefix}</ip-prefix>
                            <masklength-range>{mask_range}</masklength-range>
                        </prefix>
                        
                    </prefixes>
                    
                </prefix-set>
            </prefix-sets>
        </defined-sets>
    </routing-policy>
</config>
"""

# Create an XML body to execute the save operation
# save_body = """
# <ietf-netconf:commit xmlns:ietf-netconf="urn:ietf:params:xml:ns:netconf:base:1.0"/>
# """

set_prefixset = {
    'name': 'PS_' + input('Enter prefix set name: '),
    'prefix': input('Enter prefix (e.g. 1.1.1.0/24): '),
    'mask_range': input('Enter prefix range (e.g. 25..32 or exact): ')
}

# Create netconf data format for interface
netconf_data = netconf_prefixset_template.format(
    name=set_prefixset['name'],
    prefix=set_prefixset['prefix'],
    mask_range=set_prefixset['mask_range'],
)

print(f'Config payload to be sent over NETCONF\n {netconf_data}')
print(f'Opening NETCONF connection to {device}')

# Open connection to device with ncclient
with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    device_params=device['device_params'],
    hostkey_verify=False,
) as m:

    print('Locking running datastore.')
    netconf_lockrun_reply = m.lock(target='running')
    print(xml.dom.minidom.parseString(netconf_lockrun_reply.xml).toprettyxml())
    print('Locking candidate datastore.')
    netconf_lockcandidate_reply = m.lock(target='candidate')
    print(xml.dom.minidom.parseString(netconf_lockcandidate_reply.xml).toprettyxml())

    # with m.locked(target='running'):
    print('Sending edit operation to device.')
    netconf_set_reply = m.edit_config(netconf_data, target='candidate')
    print(xml.dom.minidom.parseString(netconf_set_reply.xml).toprettyxml())

    print('Sending RPC commit operation to device.')
    # netconf_save_reply = m.dispatch(xml_.to_ele(save_body))
    netconf_save_reply = m.commit()
    print(xml.dom.minidom.parseString(netconf_save_reply.xml).toprettyxml())

    print('Unlocking candidate datastore.')
    netconf_unlockcandidate_reply = m.unlock(target='candidate')
    print(xml.dom.minidom.parseString(netconf_unlockcandidate_reply.xml).toprettyxml())
    print('Unlocking running datastore.')
    netconf_unlockrun_reply = m.unlock(target='running')
    print(xml.dom.minidom.parseString(netconf_unlockrun_reply.xml).toprettyxml())

# TODO Generate XML template dynamically from YANG file
# TODO Only one YAML file will be the source for get/set/delete operation
# TODO Dynamic template generator for each operation get/set/delete
# TODO Before operation is committed for delete/set the confirmation from user is necessary,
#      confirmation will be trigger for commit.
# MOVED to project prefix-set_skiver
