from ncclient import manager, xml_
import xml.dom.minidom

import env_lab

device = env_lab.CISCO_SANDBOX_XR

# XML body to delete specific prefix-set
netconf_prefixset_template_oc = """
<config xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
    <routing-policy xmlns="http://openconfig.net/yang/routing-policy">
        <defined-sets>
            <prefix-sets>
                <prefix-set nc:operation="delete">
                    <prefix-set-name>{name}</prefix-set-name>
                </prefix-set>
            </prefix-sets>
        </defined-sets>
    </routing-policy>
</config>
"""

# XML body to delete specific prefix-set
netconf_prefixset_template_cisco = """
<config xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0">
    <routing-policy xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg">
            <sets>
                <prefix-sets>
                    <prefix-set nc:operation="delete">
                        <set-name>{name}</set-name>
                    </prefix-set>
                </prefix-sets>
            </sets>
    </routing-policy>
</config>
"""


# Create an XML body to execute the save operation
# save_body = """
# <ietf-netconf:commit xmlns:ietf-netconf="urn:ietf:params:xml:ns:netconf:base:1.0"/>
# """

delete_prefixset = {
    'name': input('Enter prefix set name to delete: '),
}

# Create netconf data format for interface (Using OC/Cisco format)
# netconf_data = netconf_prefixset_template_cisco.format(
netconf_data = netconf_prefixset_template_oc.format(
    name=delete_prefixset['name'],
)

print(f'Config payload to be sent over NETCONF\n {netconf_data}')
print(f'Opening NETCONF connection to {device}')

# Open connection to device with ncclient
with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    device_params=device['device_params'],
    hostkey_verify=False,
) as m:

    print('Locking running datastore.')
    netconf_lockrun_reply = m.lock(target='running')
    print(xml.dom.minidom.parseString(netconf_lockrun_reply.xml).toprettyxml())
    print('Locking candidate datastore.')
    netconf_lockcandidate_reply = m.lock(target='candidate')
    print(xml.dom.minidom.parseString(netconf_lockcandidate_reply.xml).toprettyxml())

    # with m.locked(target='running'):
    print('Sending delete operation to device.')
    netconf_delete_reply = m.edit_config(netconf_data, target='candidate')
    print(xml.dom.minidom.parseString(netconf_delete_reply.xml).toprettyxml())

    print('Sending RPC commit operation to device.')
    # netconf_save_reply = m.dispatch(xml_.to_ele(save_body))
    netconf_save_reply = m.commit()
    print(xml.dom.minidom.parseString(netconf_save_reply.xml).toprettyxml())

    print('Unlocking candidate datastore.')
    netconf_unlockcandidate_reply = m.unlock(target='candidate')
    print(xml.dom.minidom.parseString(netconf_unlockcandidate_reply.xml).toprettyxml())
    print('Unlocking running datastore.')
    netconf_unlockrun_reply = m.unlock(target='running')
    print(xml.dom.minidom.parseString(netconf_unlockrun_reply.xml).toprettyxml())
