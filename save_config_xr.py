from ncclient import manager, xml_
import xmltodict
import xml.dom.minidom

import env_lab

device = env_lab.CISCO_SANDBOX_XR

# Create an XML body to execute the save operation
save_body = """
<ietf-netconf:commit xmlns:ietf-netconf="urn:ietf:params:xml:ns:netconf:base:1.0"/>
"""

print(f'Opening NETCONF connection to {device}')

# Open connection to device with ncclient
with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    device_params=device['device_params'],
    hostkey_verify=False,
) as m:
    print('Sending RPC operation to device.')
    netconf_reply = m.dispatch(xml_.to_ele(save_body))

print("Here is the raw XML data returned from the device.\n")
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())
