import env_lab
from ncclient import manager

device = env_lab.SW04

with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
) as m:

    # Print device capabilities
    for capability in m.server_capabilities:
        print(capability)

