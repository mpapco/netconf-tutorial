from ncclient import manager
import xml.dom.minidom

import env_lab

device = env_lab.SW04

netconf_vlan_oc_template = """
<config>
    <vlans xmlns="http://openconfig.net/yang/vlan">
        <vlan operation="create">
            <vlan-id>777</vlan-id>
            <config>
                <vlan-id>777</vlan-id>
                <name>AUTO_configured_VLAN</name>
            </config>
        </vlan>
    </vlans>
</config>
"""

netconf_vlan_cisco_template = """
<config>
    <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
        <vlan>
            <vlan-list xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-vlan" operation="create">
                <id>777</id>
                <name>AUTO_configured_VLAN</name>
            </vlan-list>
        </vlan>
    </native>
</config>
"""

# Create netconf data format for interface
# netconf_data = netconf_vlan_oc_template .format()
netconf_data = netconf_vlan_cisco_template .format()

print(f'Config payload to be sent over NETCONF\n {netconf_data}')
print(f'Opening NETCONF connection to {device}')

# Open connection to device with ncclient
with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
) as m:

    print('Sending <edit-config> to device.')
    netconf_reply = m.edit_config(netconf_data, target='running')

print("Here is the raw XML data returned from the device.\n")
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())
