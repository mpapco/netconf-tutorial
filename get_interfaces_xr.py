import env_lab
from ncclient import manager
import xml.dom.minidom
import xmltodict

device = env_lab.CISCO_SANDBOX_XR

netconf_filter = """
<filter>
   <interface-configurations xmlns="http://cisco.com/ns/yang/Cisco-IOS-XR-ifmgr-cfg" xmlns:ipv4-io-cfg="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-io-cfg">
     <interface-configuration>
     </interface-configuration>
   </interface-configurations>
</filter>
"""

print(f"Opening NETCONF Connection to {device['host']}")

with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
    device_params=device['device_params']
) as m:

    print('Sending a <get-config> operation to the device.')
    # Make a NETCONF <get-config> query using the filter
    netconf_reply = m.get_config(source='running', filter=netconf_filter)

print("Here is the raw XML data returned from the device.\n")
# Print out the raw XML that returned
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())
print('')

# Parse the returned XML to an Ordered Dictionary
netconf_data = xmltodict.parse(netconf_reply.xml)["rpc-reply"]["data"]

# Create a list of interfaces
interfaces = netconf_data["interface-configurations"]["interface-configuration"]

print("The interface status of the device is: ")
# Loop over interfaces and report status
for interface in interfaces:
    print(f"Interface {interface['interface-name']}")
    if 'shutdown' in interface:
        print(f"\tStatus: shutdown")
    else:
        print(f"\tStatus: enabled")
    if 'ipv4-network' in interface:
        print(f"\tAddress: {interface['ipv4-network']['addresses']['primary']['address']}"
              f"/{interface['ipv4-network']['addresses']['primary']['netmask']}")
print("\n")


