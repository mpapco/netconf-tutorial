from ncclient import manager
import xml.dom.minidom

import env_lab

device = env_lab.CISCO_SANDBOX_XE

# IETF Interface Types
INTERFACE_TYPES = {
    'loopback': 'ianaift:softwareLoopback',
    'ethernet': 'ianaift:ethernetCsmacd',
    'framerelay': 'ianaift:frameRelay',
    'tunnel': 'ianaift:tunnel',
    'vlan': 'ianaift:l2vlan',
    'l3vlan': 'ianaift:l3ipvlan',
    'mplstunnel': 'ianaift:mplsTunnel',
    'mpls': 'ianaift:mpls',
}

# XML configuration template
netconf_interface_template = """
<config>
    <interfaces xmlns="urn:ietf:params:xml:ns:yang:ietf-interfaces">
        <interface>
            <name>{name}</name>
            <description>{desc}</description>
            <type xmlns:ianaift="urn:ietf:params:xml:ns:yang:iana-if-type">
                {type}
            </type>
            <enabled>{status}</enabled>
            <ipv4 xmlns="urn:ietf:params:xml:ns:yang:ietf-ip">
                <address>
                    <ip>{ip_address}</ip>
                    <netmask>{mask}</netmask>
                </address>
            </ipv4>
        </interface>
    </interfaces>
</config>
"""

new_loopback = {
    'name': 'Loopback' + input('Enter Loopback number: '),
    'desc': 'Created by Automated script',
    'type': INTERFACE_TYPES['loopback'],
    'status': 'true',
    'ip_address': input('Enter IP address: '),
    'mask': input('Enter network mask: '),
}

# Create netconf data format for interface
netconf_data = netconf_interface_template.format(
    name=new_loopback['name'],
    desc=new_loopback['desc'],
    type=new_loopback['type'],
    status=new_loopback['status'],
    ip_address=new_loopback['ip_address'],
    mask=new_loopback['mask'],
)

print(f'Config payload to be sent over NETCONF\n {netconf_data}')
print(f'Opening NETCONF connection to {device}')

# Open connection to device with ncclient
with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
) as m:

    print('Sending <edit-config> to device.')
    netconf_reply = m.edit_config(netconf_data, target='running')

print("Here is the raw XML data returned from the device.\n")
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())

