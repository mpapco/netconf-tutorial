from ncclient import manager
import xml.dom.minidom

import env_lab

device = env_lab.SW04

# Cisco-IOS-XE-ip.yang
# Cisco-IOS-XE-dhcp.yang
netconf_dhcp_cisco_template = """
<config>
    <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
        <ip>
            <dhcp operation="merge">
                <excluded-address xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-dhcp">
                    <low-address-list>
                        <low-address>1.2.3.10</low-address>
                    </low-address-list>
                    <low-high-address-list>
                        <low-address>1.2.3.4</low-address>
                        <high-address>1.2.3.7</high-address>
                    </low-high-address-list>
                </excluded-address>
                <pool xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-dhcp">
                    <id>AUTO_POOL</id>
                    <network>
                        <primary-network>
                            <number>1.2.3.0</number>
                            <mask>/24</mask>
                        </primary-network>
                    </network>
                    <dns-server>
                        <dns-server-list>8.8.8.8</dns-server-list>
                        <dns-server-list>8.8.4.4</dns-server-list>
                    </dns-server>
                    <domain-name>ngena.sk</domain-name>
                    <default-router>
                        <default-router-list>1.1.1.1</default-router-list>
                    </default-router>
                    <lease>
                        <lease-value>
                            <hours>10</hours>
                        </lease-value>
                    </lease>
                </pool>
            </dhcp>
        </ip>
    </native>
</config>    
"""


# Create netconf data format for interface
netconf_data = netconf_dhcp_cisco_template.format()

print(f'Config payload to be sent over NETCONF\n {netconf_data}')
print(f'Opening NETCONF connection to {device}')

# Open connection to device with ncclient
with manager.connect(
        host=device['host'],
        port=device['netconf_port'],
        username=device['username'],
        password=device['password'],
        hostkey_verify=False,
) as m:
    print('Sending <edit-config> to device.')
    netconf_reply = m.edit_config(netconf_data, target='running')

print("Here is the raw XML data returned from the device.\n")
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())