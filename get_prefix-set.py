import env_lab
from ncclient import manager
import xml.dom.minidom
import xmltodict

device = env_lab.CISCO_SANDBOX_XR

netconf_filter = """
<filter>
    <routing-policy xmlns="http://openconfig.net/yang/routing-policy">
        <defined-sets>
            <prefix-sets>
                <prefix-set>
                    <prefix-set-name>PS_TEST1</prefix-set-name>
                </prefix-set>
            </prefix-sets>
        </defined-sets>
    </routing-policy>
</filter>
"""

print(f"Opening NETCONF Connection to {device['host']}")

with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    device_params=device['device_params'],
    hostkey_verify=False,
) as m:

    print('Sending a <get-config> operation to the device.')
    # Make a NETCONF <get-config> query using the filter
    netconf_reply = m.get_config(source='running', filter=netconf_filter)

print("Here is the raw XML data returned from the device.\n")
# Print out the raw XML that returned
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())
# print('')

try:
    # Parse the returned XML to an Ordered Dictionary
    netconf_data = xmltodict.parse(netconf_reply.xml)['rpc-reply']['data']['routing-policy']\
                                                    ['defined-sets']
    # Create a list of interfaces
    prefix_sets = netconf_data["prefix-sets"]

    print("Configured prefix-sets:")
    # Loop over interfaces and report status
    # print(prefix_sets)
    for prefix_set in prefix_sets['prefix-set']:
        print(prefix_set['prefix-set-name'])
        try:
            for prefix in prefix_set['prefixes']:
                # print(f"type: {type(prefix_set['prefixes']['prefix'])}")
                # print(f"\t{prefix_set['prefixes']['prefix']}")
                if type(prefix_set['prefixes']['prefix']) is list:
                    for ip_prefix in prefix_set['prefixes']['prefix']:
                        print(f"\t{ip_prefix['ip-prefix']}/{ip_prefix['masklength-range']}")
                        print(f"\t{ip_prefix['config']['ip-prefix']}/{ip_prefix['config']['masklength-range']} config")
                else:
                    print(f"\t{prefix_set['prefixes']['prefix']['ip-prefix']}/"
                          f"{prefix_set['prefixes']['prefix']['masklength-range']}")
                    print(f"\t{prefix_set['prefixes']['prefix']['config']['ip-prefix']}/"
                          f"{prefix_set['prefixes']['prefix']['config']['masklength-range']} config")
        except KeyError:
            print(f"[KeyError]  prefix set is empty.")
except TypeError as e:
    print(f"[TypeError] No prefix set configured. {e}")

# FIXME Issue when only one prefix set with
