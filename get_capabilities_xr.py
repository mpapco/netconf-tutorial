import env_lab
from ncclient import manager

device = env_lab.CISCO_SANDBOX_XR

with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
    device_params=device['device_params']
) as m:

    # Print device capabilities
    for capability in m.server_capabilities:
        print(capability)

