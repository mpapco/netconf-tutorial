from ncclient import manager
import xml.dom.minidom

import env_lab

device = env_lab.SW04

netconf_staticroute_oc_template_NOT_WORKING = """
<config>
    <local-routes xmlns="http://openconfig.net/yang/local-routing">
        <static-routes>
            <static>
                <prefix>10.10.10.0/24</prefix>    
                <next-hops>
                    <next-hop>
                        <index>default</index>
                        <config>
                            
                            <next-hop>1.2.3.4</next-hop>
                        </config>
                    </next-hop>
                </next-hops>
            </static>    
        </static-routes>
    </local-routes>
</config>
"""

netconf_staticroute_cisco_template = """
<config>
    <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
        <ip>
            <route>
                <ip-route-interface-forwarding-list operation="replace">
                    <prefix>10.10.10.0</prefix>
                    <mask>255.255.255.0</mask>
                    <fwd-list>
                        <fwd>1.6.7.8</fwd>
                        <name>AUTO</name>
                    </fwd-list>
                </ip-route-interface-forwarding-list>
            </route>
        </ip>
    </native>
</config>    
"""

# Create netconf data format for interface
# netconf_data = netconf_staticroute_oc_template .format()
netconf_data = netconf_staticroute_cisco_template .format()

print(f'Config payload to be sent over NETCONF\n {netconf_data}')
print(f'Opening NETCONF connection to {device}')

# Open connection to device with ncclient
with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
) as m:

    print('Sending <edit-config> to device.')
    netconf_reply = m.edit_config(netconf_data, target='running')

print("Here is the raw XML data returned from the device.\n")
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())