from ncclient import manager
import xml.dom.minidom

import env_lab

device = env_lab.CISCO_SANDBOX_XE

netconf_prefixlist_template = """
<config>
    <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native">
        <ip>
            <prefix-list>
                <prefixes operation="delete">
                    <name>{name}</name>
                </prefixes>
            </prefix-list>
        </ip>
    </native>
</config>
"""

del_prefixlist = {
    'name': 'PL_' + input('Enter prefix list name to delete: '),
}


# Create netconf data format for interface
netconf_data = netconf_prefixlist_template.format(
    name=del_prefixlist['name'],
)

print(f'Config payload to be sent over NETCONF\n {netconf_data}')
print(f'Opening NETCONF connection to {device}')

# Open connection to device with ncclient
with manager.connect(
    host=device['host'],
    port=device['netconf_port'],
    username=device['username'],
    password=device['password'],
    hostkey_verify=False,
) as m:

    print('Sending delete operation to device.')
    netconf_reply = m.edit_config(netconf_data, target='running')

print("Here is the raw XML data returned from the device.\n")
print(xml.dom.minidom.parseString(netconf_reply.xml).toprettyxml())
